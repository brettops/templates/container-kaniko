# container-kaniko template

[![pipeline status](https://gitlab.com/brettops/templates/container-kaniko/badges/main/pipeline.svg)](https://gitlab.com/brettops/templates/container-kaniko/-/commits/main)

Publish a container with Kaniko.

For more information, see the [container](https://gitlab.com/brettops/pipelines/container) pipeline.

## Usage

- Fork the project and clone locally

- Run `pre-commit install` (get [pre-commit](https://pre-commit.com/) if you don't have it)

- Update the `IMAGE` variable in the makefile

- Make your changes to the Dockerfile

- Enable the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) for your project
