FROM ${CONTAINER_PROXY}alpine:3.11

# hadolint ignore=DL3018
RUN apk add --no-cache bash

CMD ["/bin/bash"]
